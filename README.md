# infra.self.eng-svcs

> Hosted web services for AlexLab Cloud engineering

Container definitions and other resources for web applications that support AlexLab Cloud's development efforts.

This infrastructure requires a server capable of installing a container runtime.

## Usage

### Download

#### HTTPS

On your server, run `git clone https://gitlab.com/alexlab-cloud/infra/self/eng-svcs`

### Environment

Environment variables are provided by [dotenv-vault](https://github.com/dotenv-org).

1. `pnpx dotenv-vault@latest login` to trigger log in using the [`.env.vault`] file present in source
2. Log in at the [vault.dotenv.org](https://vault.dotenv.org) URL provided by (1)
3. `pnpx dotenv-vault@latest pull <ENVIRONMENT>`, where `<ENVIRONMENT>` is one of [`development`, `production`]

This should produce a `.env.<ENVIRONMENT>` file in the project root

### [containers/](./containers/)

Compose definitions for various services are provided in the [`containers/`](./containers/) directory.

<hr>

<sub>Emoji used for repository logo designed by <a href="https://openmoji.org/">OpenMoji</a> – the open-source emoji and icon project. License: CC BY-SA 4.0</sub>

<hr>
