# Authelia Secrets

This directory should contain Authelia secret files once this project is deployed.

See the [docs][links.authelia-docs.secrets] for more information.

<!-- Links -->
[links.authelia-docs.secrets]: https://www.authelia.com/configuration/methods/secrets/
