#!/usr/bin/env bash

# Finds all environment variable references in compose files to make
# it easier to maintain dotenv files.

# TODO: only extract variable name string (no filepath) + only unique results

# shellcheck disable=SC2016
find "./containers" -type f -name "*.yml" -exec grep -oP '(?<=\${).*?(?=})' {} + | sort -u
