#!/usr/bin/env bash

# Hash a password using Authelia's docker image

docker run authelia/authelia:latest authelia hash-password "$1"
