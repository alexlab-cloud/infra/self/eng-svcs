#!/usr/bin/env bash

# Run all apps/ services

COMPOSE_FILES="$(find . | grep "./containers/apps.*docker-compose.*.yml")"

for STACK in $COMPOSE_FILES; do
    echo "Building ${STACK}"
    docker compose -f "${STACK}" --env-file ./.env.vault up -d
done
