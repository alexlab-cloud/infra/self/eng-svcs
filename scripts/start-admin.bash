#!/usr/bin/env bash

# Run all admin/ services

COMPOSE_FILES="$(find . | grep "containers/admin.*docker-compose.*.yml")"

for STACK in $COMPOSE_FILES; do
    echo "Building ${STACK}"
    docker compose -f "${STACK}" --env-file ./.env.production up -d
done
