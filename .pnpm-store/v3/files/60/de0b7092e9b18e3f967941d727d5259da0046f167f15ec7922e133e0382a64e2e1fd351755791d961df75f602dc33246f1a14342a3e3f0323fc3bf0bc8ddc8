"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.WhoamiService = void 0;
const tslib_1 = require("tslib");
const axios_1 = tslib_1.__importDefault(require("axios"));
const vars_1 = require("../vars");
const core_1 = require("@oclif/core");
const log_service_1 = require("../services/log-service");
const abort_service_1 = require("../services/abort-service");
class WhoamiService {
    constructor(attrs = {}) {
        this.cmd = attrs.cmd;
        this.dotenvMe = attrs.dotenvMe;
        this.log = new log_service_1.LogService({ cmd: attrs.cmd });
        this.abort = new abort_service_1.AbortService({ cmd: attrs.cmd });
    }
    async run() {
        if (vars_1.vars.missingEnvVault) {
            this.abort.missingEnvVault();
        }
        if (vars_1.vars.emptyEnvVault) {
            this.abort.emptyEnvVault();
        }
        if (vars_1.vars.missingEnvMe(this.dotenvMe)) {
            this.abort.missingEnvMe();
        }
        if (vars_1.vars.emptyEnvMe(this.dotenvMe)) {
            this.abort.emptyEnvMe();
        }
        await this.whoami();
    }
    async whoami() {
        const options = {
            method: 'POST',
            headers: { 'content-type': 'application/json' },
            data: {
                DOTENV_ME: this.meUid,
                DOTENV_VAULT: vars_1.vars.vaultValue,
            },
            url: this.url,
        };
        try {
            const resp = await (0, axios_1.default)(options);
            const email = resp.data.data.email;
            this.log.plain(email);
        }
        catch (error) {
            core_1.CliUx.ux.action.stop('aborting');
            let errorMessage = null;
            let errorCode = 'WHOAMI_ERROR';
            let suggestions = [];
            errorMessage = error;
            if (error.response) {
                errorMessage = error.response.data;
                if (error.response.data && error.response.data.errors && error.response.data.errors[0]) {
                    const error1 = error.response.data.errors[0];
                    errorMessage = error1.message;
                    if (error1.code) {
                        errorCode = error1.code;
                    }
                    if (error1.suggestions) {
                        suggestions = error1.suggestions;
                    }
                }
            }
            this.abort.error(errorMessage, { code: errorCode, ref: '', suggestions: suggestions });
        }
    }
    get url() {
        return vars_1.vars.apiUrl + '/whoami';
    }
    get meUid() {
        return this.dotenvMe || vars_1.vars.meValue;
    }
}
exports.WhoamiService = WhoamiService;
