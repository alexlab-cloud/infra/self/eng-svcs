"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@oclif/core");
const keys_service_1 = require("../services/keys-service");
class Keys extends core_1.Command {
    async run() {
        const { args, flags } = await this.parse(Keys);
        const environment = args.environment;
        const dotenvMe = flags.dotenvMe;
        const yes = flags.yes;
        await new keys_service_1.KeysService({ cmd: this, environment: environment, dotenvMe: dotenvMe, yes: yes }).run();
    }
}
exports.default = Keys;
Keys.description = 'List .env.vault decryption keys';
Keys.examples = [
    '<%= config.bin %> <%= command.id %>',
];
Keys.args = [
    {
        name: 'environment',
        required: false,
        description: 'Set environment to fetch key(s) from. Defaults to all environments',
        hidden: false,
    },
];
Keys.flags = {
    dotenvMe: core_1.Flags.string({
        char: 'm',
        description: 'Pass .env.me (DOTENV_ME) credential directly (rather than reading from .env.me file)',
        hidden: false,
        multiple: false,
        env: 'DOTENV_ME',
        required: false,
    }),
    yes: core_1.Flags.boolean({
        char: 'y',
        description: 'Automatic yes to prompts. Assume yes to all prompts and run non-interactively.',
        hidden: false,
        required: false,
        default: false,
    }),
};
